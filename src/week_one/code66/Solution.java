package week_one.code66;

public class Solution {
    public int[] plusOne(int[] digits) {
        for(int i = digits.length - 1 ; i >= 0 ;i--){
            digits[i]++;
            //让当前数对10取余数
            digits[i] = digits[i] % 10;
            //如果对10取余数不为0说明不需要进位，直接返回
            //如果为0，说明需要进位，然后再次遍历接着+1（进位）
            if(digits[i] != 0){
                return digits;
            }
        }
        //如果循环结束了还没返回，说明整个数组需要进位，就是999这种，循环出来变成了000
        //所以只需要开一个新数组，在第一位放一个1就行， 999 变 000 ，然后000前面放1 变 1000
        digits = new  int[digits.length + 1];
        digits[0] = 1;
        return digits;
    }
}
