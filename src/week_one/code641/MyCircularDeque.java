package week_one.code641;

class MyCircularDeque {
    //用来存数据的静态数组
    private int[] arr;
    //数组大小
    private int size;
    //收尾指针
    private int head;
    private int tail;

    public MyCircularDeque(int k) {
        //初始化基本数据
        arr = new int[k];
        size = 0;
        head = 0;
        tail = k - 1;
    }

    public boolean insertFront(int value) {
        // 1:头指针左移一位
        // 1.1：因为是循环队列，所以头指针为0的时候，左移一位就会走到数组末尾
        // 2:在头指针处赋值
        // size + 1
        if(!isFull()){
            // head 如果等于0 ，则head = 数组长度-1，否则head = head -1
            head = head == 0 ? arr.length - 1 : head - 1;
            arr[head] = value;
            size++;
            return true;
        }else {
            return false;
        }
    }

    public boolean insertLast(int value) {
        // 1: 尾指针右移一位
        // 1.1：因为是循环队列，所以尾指针尾数组长度-1的时候，右移一位会变成0
        // 2:在尾指针处赋值
        // size + 1
        if(!isFull()){
            // tail 如果等于数组长度-1 ，则tail = 0 ，否则tail = tail+ 1
            tail = tail == arr.length - 1 ? 0 : tail + 1;
            arr[tail] = value;
            size++;
            return true;
        }else {
            return false;
        }
    }

    public boolean deleteFront() {
        //头指针右移，size -1
        if(!isEmpty()){
            head = head == arr.length - 1 ? 0 : head + 1;
            size--;
            return true;
        }else {
            return false;
        }

    }

    public boolean deleteLast() {
        //尾指针左移
        if(!isEmpty()){
            tail = tail == 0 ? arr.length - 1 : tail -1;
            size--;
            return true;
        }else {
            return false;
        }
    }

    public int getFront() {
        if(isEmpty()){
            return -1;
        }
        return arr[head];

    }

    public int getRear() {
        if(isEmpty()){
            return -1;
        }
        return arr[tail];
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public boolean isFull() {
        return size == arr.length;
    }
}

/**
 * Your MyCircularDeque object will be instantiated and called as such:
 * MyCircularDeque obj = new MyCircularDeque(k);
 * boolean param_1 = obj.insertFront(value);
 * boolean param_2 = obj.insertLast(value);
 * boolean param_3 = obj.deleteFront();
 * boolean param_4 = obj.deleteLast();
 * int param_5 = obj.getFront();
 * int param_6 = obj.getRear();
 * boolean param_7 = obj.isEmpty();
 * boolean param_8 = obj.isFull();
 */
