package week_two.code560;

public class Solution {
    public int subarraySum(int[] nums, int k) {
        //首先是前缀和的经典代码模板
        int[] s = new int[nums.length + 1];
        s[0] = 0;
        for(int i=1;i <= nums.length;i++){
            s[i] = s[i-1] + nums[i-1];
        }
        //双重遍历前缀和
        int res = 0;
        for(int i = 0;i < s.length;i++){
            for(int j = i + 1;j < s.length;j++){
                if(s[j] - s[i] == k){
                    res++;
                }

            }
        }
        return res;

    }
}
